<?php

/**
 * @file
 * Contains Drupal\spotlight_dpv_map\Form\SubscriptionUserDPVForm.
 */

namespace Drupal\spotlight_dpv_map\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
//use Drupal\user\Entity\User;

/**
 * Class SubscriptionUserDPVForm.
 *
 * @package Drupal\spotlight_dpv_map\Form
 */
class SubscriptionUserDPVForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'spotlight_dpv_map.subscr_user_dpv_form'
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'subscr_user_dpv_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
//    $config = $this->config('spotlight_dpv_map.subscr_user_dpv_form');


    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

//    $request_time = REQUEST_TIME;
//    $last_cron_run = $form_state->getValue('last_cron_run');
//    $arr_last_cron_run = [];
//    $pieces = explode("|", $last_cron_run);
//    if ( count($pieces) < 3 ) {
//    }
//    else {
//      array_shift($pieces);
//    }
//    $arr_last_cron_run = $pieces;
//    $arr_last_cron_run[] = date("d.m.Y H:i:s", $request_time);
//
//    $last_cron_run = implode(" | ", $arr_last_cron_run);
//
//    $this->config('spotlight_user_notifications.sun_config')
////      ->set('last_cron_run', $form_state->getValue('last_cron_run'))
//      ->set('last_cron_run', $last_cron_run)
//      ->save();
//
//    $queue = \Drupal::queue('spotlight_notifications');
//    $op = [];
//    while ( $item = $queue->claimItem() ) {
//      $op[] = ['\Drupal\spotlight_user_notifications\SunProcessorService::notify_user_access', array($item->data)];
//      $queue->deleteItem($item);
//    }
//    if (!empty($op)) {
//      $batch = array(
//        'title' => t('Notify User Activity...'),
//        'operations' => $op,
//        'finished' => '\Drupal\spotlight_user_notifications\SunProcessorService::notificationFinishedCallback',
//      );
//      batch_set($batch);
//    }


//    $data["uid"] = $userinfo['id'];
//    $data["sdk_ids"] = $subscriptions;
//
//    $user = "admin";
//    $password = "TSVi1coHiG.";
//    $base_url = "https://services.spotlight-verlag.de";
//
//    $client = \Drupal::httpClient();
//    try {
//      // determine if $this->client is exist
//      if (empty($client)) {
//        throw new \Exception("The Guzzle HTTP client is not set.");
//      }
//
//      if (empty($user) || empty($password) || empty($base_url)) {
//        throw new \Exception("User or Password or Base URL is not properly set.");
//      }
//
//      $z = json_encode($data, true);
//
//      $response = $client->post($base_url.'/account/register/dpv/products.json',
//        ['auth' => [$user, $password],
//          'body' => $z
//        ]
//      );
//
//      print_r($response->getBody()->getContents());
//      die('a');
//      if (empty($response)) {
//        return FALSE;
//      }
////        return json_decode($response->getBody()->getContents(), true);
//    }
//    catch(\Exception $e) {
//      \Drupal::logger('SpotlightVocabularyProcessor :: postUserVocabulary')->warning($e->getMessage());
//      return FALSE;
//    }


  }
}
