<?php

/**
 * @file
 * Contains Drupal\spotlight_dpv_map\Form\ConfigServicesForm.
 */

namespace Drupal\spotlight_dpv_map\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormState;

/**
 * Class ConfigServicesForm.
 *
 * @package Drupal\spotlight_dpv_map\Form
 */
class ConfigServicesForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'spotlight_dpv_map.configservices_config'
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'config_services_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('spotlight_dpv_map.configservices_config');
    $form['user'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('User'),
      '#description' => $this->t(''),
      '#default_value' => $config->get('user'),
    );
    $form['password'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#description' => $this->t(''),
      '#default_value' => $config->get('password'),
    );
    $form['base_url'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Base URL'),
      '#description' => $this->t(''),
      '#default_value' => $config->get('base_url'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('spotlight_dpv_map.configservices_config')
      ->set('user', $form_state->getValue('user'))
      ->set('password', $form_state->getValue('password'))
      ->set('base_url', $form_state->getValue('base_url'))
      ->save();
  }

}
