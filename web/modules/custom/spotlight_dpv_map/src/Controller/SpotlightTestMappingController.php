<?php

namespace Drupal\spotlight_dpv_map\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class SpotlightTestMappingController.
 */
class SpotlightTestMappingController extends ControllerBase {

  /**
   * Mapping_test.
   *
   * @return string
   *   Return Hello string.
   */
  public function mapping_test() {

    $container = \Drupal::getContainer();
    $service = $container->get('spotlight_dpv_map.default');

    $usr = $service->getRawArray();

    $service->mapping($usr['roles']);

    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: mapping_test')
    ];
  }

}
