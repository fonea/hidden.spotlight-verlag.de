<?php

namespace Drupal\spotlight_dpv_map;
use Drupal\Core\Database\Driver\mysql\Connection;
use Spotlight\Product\Entity\Product;
use GuzzleHttp\Client;


/**
 * Class SpotlightDpvMappingService.
 */
class SpotlightDpvMappingService implements SpotlightDpvMappingServiceInterface {

  /**
   * Drupal\Core\Database\Driver\mysql\Connection definition.
   *
   * @var \Drupal\Core\Database\Driver\mysql\Connection
   */
  protected $database;
  /**
   * GuzzleHttp\Client definition.
   *
   * @var GuzzleHttp\Client
   */
  protected $http_client;
  protected $config;

  /**
   * Constructs a new SpotlightDpvMappingService object.
   */
  public function __construct(Connection $database, Client $http_client) {
    $this->database = $database;
    $this->http_client = $http_client;
    $this->getConfig();
  }

  function mapping($roles=[]) {

    $userRoles = \Drupal::currentUser()->getRoles();
    $possible_products = $this->dpvMappingConfig();

    if (empty($roles) || !is_array($roles)) {
      return;
    }

    if (empty($userRoles) || in_array('anonymous', $userRoles)) {
      return;
    }

    $now = time();
    $subscriptions = [];
    foreach ($roles as $entry) {
      if (preg_match('#^magento#i', $entry['product_name']) === 1) {
        continue;
      }

      $current_product = strstr($entry['product_name'], 'spotlight');
      if (array_key_exists($current_product, $possible_products)) {

        $time_end = $now;
        if (array_key_exists('time_end', $entry)) {
          $dateTime = \DateTime::createFromFormat('Y-m-d\TH:i:s e', $entry['time_end']);
          $time_end = $dateTime->format('U');
        }
        if (($time_end >= $now) && !(in_array("administrator", $userRoles) || in_array("mighty_downloader", $userRoles))) {
          $time_end = $now;
        }

        $dateTime = \DateTime::createFromFormat('Y-m-d\TH:i:s e', $entry['time_start']);
        $time_start = $dateTime->format('U');

        $product = $possible_products[$current_product];
        $product->setDate($time_start);

        if (!in_array(trim($product->id()), $subscriptions)) {
//          $subscriptions[] = trim($product->id());
          $subscriptions[] = ["id" => trim($product->id())];
        }

        $brand = $product->brand();
        $product_variant = $product->productVariant();
        $evtService = new \Spotlight\Evt\Service\EvtService();
        $nextIssueNr = $evtService->nextIssueNr($brand, $product_variant, $product->issuenr());
        $product->setIssueNr($nextIssueNr);
        $evt = $product->evt();

        $i = 0;
        while($i < 1000 && $evt < $time_end  ) {
          $i++;

          if (!in_array(trim($product->id()), $subscriptions)) {
//            $subscriptions[] = trim($product->id());
            $subscriptions[] = ["id" => trim($product->id())];
          }
          $nextIssueNr = $evtService->nextIssueNr($brand, $product_variant, $product->issuenr());
          $product->setIssueNr($nextIssueNr);
          $evt = $product->evt();

        }
      }
    }
    return $subscriptions;
  }

  function getRawArray() {
    $array = array (
      'id' => 12200546,
      'email' => 'f.onea@spotlight-verlag.de',
      'roles' =>
        array (
          0 =>
            array (
              'user_id' => 12200546,
              'product_id' => '3781109',
              'product_name' => 'kimba_spotlight_adesso_digital',
              'product_title' => 'Schaltet Pressmatrix und BIC frei.',
              'product_description' => 'Schaltet Pressmatrix und BIC frei.',
              'source' => 'KIMBA',
              'subscription_number' => 'Freizugang',
              'booked_from' => 'sso',
              'time_start' => '2017-06-01T00:00:00+0200',
              'status' => '',
              'quantity' => 1,
              'api_type' => 'zdb',
              'billing_address' =>
                array (
                ),
              'shipping_address' =>
                array (
                ),
              'created_at' => '2017-10-04T17:54:26+0200',
            ),
          1 =>
            array (
              'user_id' => 12200546,
              'product_id' => '3781110',
              'product_name' => 'zdb_freiausgabe_spotlight_adesso_epaper',
              'product_title' => 'Freiausgabe ePaper',
              'product_description' => 'Freischaltung der aktuellen Ausgabe, damit der Endkunde sofortigen Zugriff auf das aktuell verfügbare PDF im Online-Shop erhält.',
              'source' => 'SSO',
              'subscription_number' => 'Freizugang',
              'booked_from' => 'sso',
              'time_start' => '2017-09-26T23:59:59+0200',
              'time_end' => '2017-09-27T00:00:01+0200',
              'status' => 'active',
              'quantity' => 1,
              'api_type' => 'zdb',
              'billing_address' =>
                array (
                ),
              'shipping_address' =>
                array (
                ),
              'created_at' => '2017-10-04T17:54:27+0200',
            ),
          2 =>
            array (
              'user_id' => 12200546,
              'product_id' => '3781111',
              'product_name' => 'kimba_spotlight_magazin_digital',
              'product_title' => 'Schaltet Pressmatrix und BIC frei.',
              'product_description' => 'Schaltet Pressmatrix und BIC frei.',
              'source' => 'KIMBA',
              'subscription_number' => 'Freizugang',
              'booked_from' => 'sso',
              'time_start' => '2017-01-01T00:00:00+0100',
              'time_end' => '2017-12-31T00:00:00+0100',
              'status' => '',
              'quantity' => 1,
              'api_type' => 'zdb',
              'billing_address' =>
                array (
                ),
              'shipping_address' =>
                array (
                ),
              'created_at' => '2017-10-04T18:08:14+0200',
            ),
          3 =>
            array (
              'user_id' => 12200546,
              'product_id' => '3781112',
              'product_name' => 'zdb_freiausgabe_spotlight_magazin_epaper',
              'product_title' => 'Freiausgabe ePaper',
              'product_description' => 'Freischaltung der aktuellen Ausgabe, damit der Endkunde sofortigen Zugriff auf das aktuell verfügbare PDF im Online-Shop erhält.',
              'source' => 'SSO',
              'subscription_number' => 'Freizugang',
              'booked_from' => 'sso',
              'time_start' => '2017-09-26T23:59:59+0200',
              'time_end' => '2017-09-27T00:00:01+0200',
              'status' => 'active',
              'quantity' => 1,
              'api_type' => 'zdb',
              'billing_address' =>
                array (
                ),
              'shipping_address' =>
                array (
                ),
              'created_at' => '2017-10-04T18:08:14+0200',
            ),
        ),
      'optins' =>
        array (
        ),
    );
    return $array;
  }


  function dpvMappingConfig() {

    $addesso_bid = 94;
    $bs_bid = 96;
    $spotlight_bid = 91;
    $ecos_bid = 93;
    $ecoute_bid = 92;
    $dp_bid = 97;

    $type_book = "printproduct";
    $type_audio = "audioproduct";
    $variant_audio_standard = 'standard';
    $variant_audio_express = 'express';

    $variant_magazin = 'standard';
    $variant_teacher = 'teacher';
    $variant_plus = 'plus';

    $addesso = [
      'spotlight_adesso_digital' => new Product($addesso_bid, $type_book, $variant_magazin, 'download'), //magazin
      'spotlight_adesso_epaper' => new Product($addesso_bid, $type_book, $variant_magazin, 'download'), //magazin

      'spotlight_adesso_lehrerbeilage_digital' => new Product($addesso_bid, $type_book, $variant_teacher, 'download'), //teacher
      'spotlight_adesso_lehrerbeilage_epaper' => new Product($addesso_bid, $type_book, $variant_teacher, 'download'), //teacher

      'spotlight_adesso_plus_digital' => new Product($addesso_bid, $type_book, $variant_plus, 'download'), //plus
      'spotlight_adesso_plus_epaper' => new Product($addesso_bid, $type_book, $variant_plus, 'download'), //plus

      'spotlight_adesso_audio' => new Product($addesso_bid, $type_audio, $variant_audio_standard, 'download'), //audio
    ];
    $bs = [
      'spotlight_businessspotlight_digital' => new Product($bs_bid, $type_book, $variant_magazin, 'download'), //magazin
      'spotlight_businessspotlight_epaper' => new Product($bs_bid, $type_book, $variant_magazin, 'download'), //magazin

      'spotlight_businessspotlight_lehrerbeilage_digital' => new Product($bs_bid, $type_book, $variant_teacher, 'download'), //teacher
      'spotlight_businessspotlight_lehrerbeilage_epaper' => new Product($bs_bid, $type_book, $variant_teacher, 'download'), //teacher

      'spotlight_businessspotlight_plus_digital' => new Product($bs_bid, $type_book, $variant_plus, 'download'), //plus
      'spotlight_businessspotlight_plus_epaper' => new Product($bs_bid, $type_book, $variant_plus, 'download'), //plus

      'spotlight_businessspotlight_audio' => new Product($bs_bid, $type_audio, $variant_audio_standard, 'download'), //audio
    ];
    $spotlight = [
      'spotlight_magazin_digital' => new Product($spotlight_bid, $type_book, $variant_magazin, 'download'), //magazin
      'spotlight_magazin_epaper' => new Product($spotlight_bid, $type_book, $variant_magazin, 'download'), //magazin

      'spotlight_magazin_lehrerbeilage_digital' => new Product($spotlight_bid, $type_book, $variant_teacher, 'download'), //teacher
      'spotlight_magazin_lehrerbeilage_epaper' => new Product($spotlight_bid, $type_book, $variant_teacher, 'download'), //teacher

      'spotlight_magazin_plus_digital' => new Product($spotlight_bid, $type_book, $variant_plus, 'download'), //plus
      'spotlight_magazin_plus_epaper' => new Product($spotlight_bid, $type_book, $variant_plus, 'download'), //plus

      'spotlight_magazin_audio' => new Product($spotlight_bid, $type_audio, $variant_audio_standard, 'download'), //audio
      'spotlight_magazin_express_audio' => new Product($spotlight_bid, $type_audio, $variant_audio_express, 'download'), //audio express
    ];
    $ecos = [
      'spotlight_ecos_digital' => new Product($ecos_bid, $type_book, $variant_magazin, 'download'), //magazin
      'spotlight_ecos_epaper' => new Product($ecos_bid, $type_book, $variant_magazin, 'download'), //magazin

      'spotlight_ecos_lehrerbeilage_digital' => new Product($ecos_bid, $type_book, $variant_teacher, 'download'), //teacher
      'spotlight_ecos_lehrerbeilage_epaper' => new Product($ecos_bid, $type_book, $variant_teacher, 'download'), //teacher

      'spotlight_ecos_plus_digital' => new Product($ecos_bid, $type_book, $variant_plus, 'download'), //plus
      'spotlight_ecos_plus_epaper' => new Product($ecos_bid, $type_book, $variant_plus, 'download'), //plus

      'spotlight_ecos_audio' => new Product($ecos_bid, $type_audio, $variant_audio_standard, 'download'), //audio
    ];
    $ecoute = [
      'spotlight_ecoute_digital' => new Product($ecoute_bid, $type_book, $variant_magazin, 'download'), //magazin
      'spotlight_ecoute_epaper' => new Product($ecoute_bid, $type_book, $variant_magazin, 'download'), //magazin

      'spotlight_ecoute_lehrerbeilage_digital' => new Product($ecoute_bid, $type_book, $variant_teacher, 'download'), //teacher
      'spotlight_ecoute_lehrerbeilage_epaper' => new Product($ecoute_bid, $type_book, $variant_teacher, 'download'), //teacher

      'spotlight_ecoute_plus_digital' => new Product($ecoute_bid, $type_book, $variant_plus, 'download'), //plus
      'spotlight_ecoute_plus_epaper' => new Product($ecoute_bid, $type_book, $variant_plus, 'download'), //plus

      'spotlight_ecoute_audio' => new Product($ecoute_bid, $type_audio, $variant_audio_standard, 'download'), //audio
    ];
    $dp = [
      'spotlight_deutschperfekt_digital' => new Product($dp_bid, $type_book, $variant_magazin, 'download'), //magazin
      'spotlight_deutschperfekt_epaper' => new Product($dp_bid, $type_book, $variant_magazin, 'download'), //magazin

      'spotlight_deutschperfekt_lehrerbeilage_digital' => new Product($dp_bid, $type_book, $variant_teacher, 'download'), //teacher
      'spotlight_deutschperfekt_lehrerbeilage_epaper' => new Product($dp_bid, $type_book, $variant_teacher, 'download'), //teacher

      'spotlight_deutschperfekt_plus_digital' => new Product($dp_bid, $type_book, $variant_plus, 'download'), //plus
      'spotlight_deutschperfekt_plus_epaper' => new Product($dp_bid, $type_book, $variant_plus, 'download'), //plus

      'spotlight_deutschperfekt_audio' => new Product($dp_bid, $type_audio, $variant_audio_standard, 'download'), //audio
    ];

    return array_merge($addesso, $bs, $spotlight, $ecos, $ecoute, $dp);
  }

  public function registerUserSubscriptions($data) {
    try {
      $user = $this->config->get('user');
      $password = $this->config->get('password');
      $base_url = $this->config->get('base_url');
      // determine if $this->client is exist
      if (empty($this->http_client)) {
        throw new \Exception("The Guzzle HTTP client is not set.");
      }

      if (empty($user) || empty($password) || empty($base_url)) {
        throw new \Exception("User or Password or Base URL is not properly set.");
      }

      if (!empty($data)) {

        $response = $this->http_client->post($base_url.'/account/register/dpv/products.json',
          ['auth' => [$user, $password],
            'json' => $data]
        );
      }
    }
    catch (\Exception $e) {
      \Drupal::logger('registerUserSubscriptions')->warning($e->getMessage());
    }
  }


  public function getConfig() {
    $this->config = \Drupal::config('spotlight_dpv_map.configservices_config');
  }
}


