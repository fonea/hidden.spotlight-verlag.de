<?php

/**
 * @file
 * Contains Drupal\openid_connect_sso_client\SingleSignOnClientSubscriber.
 */

namespace Drupal\openid_connect_sso_client\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Drupal\openid_connect_sso\SingleSignOnService;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Drupal\Core\Session\AnonymousUserSession;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Symfony\Component\HttpFoundation\Cookie;

/**
 * Class SingleSignOnClientSubscriber.
 *
 * @package Drupal\openid_connect_sso_client
 */
class SingleSignOnClientSubscriber implements EventSubscriberInterface {

  /**
   * Drupal\openid_connect_sso\SingleSignOnService definition.
   *
   * @var Drupal\openid_connect_sso\SingleSignOnService
   */
  protected $openid_connect_sso_default;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;
  /**
   * Constructor.
   */
  public function __construct(SingleSignOnService $openid_connect_sso_default,  AccountInterface $account) {
    $this->openid_connect_sso_default = $openid_connect_sso_default;
    $this->account = $account;
  }

  public function onResponseInit(FilterResponseEvent $event) {

    $exclude_sso = ['local.de', 'sandbox.spotlight-verlag.de'];

    foreach ($exclude_sso as $item) {
      if (strpos($_SERVER['HTTP_HOST'], $item) !== false) {
        return;
      }
    }


//    $cookie_domain = $this->openid_connect_sso_default->openid_connect_sso_get_cookie_domain();
//    $oposite_name_as_key = $this->openid_connect_sso_default->openid_connect_sso_get_cookie_name('logout', TRUE);
//    $event->getResponse()->headers->clearCookie($oposite_name_as_key, '/', $cookie_domain);
//
//    $cookie_domain = $this->openid_connect_sso_default->openid_connect_sso_get_cookie_domain();
//    $oposite_name_as_key = $this->openid_connect_sso_default->openid_connect_sso_get_cookie_name('login', TRUE);
//    $event->getResponse()->headers->clearCookie($oposite_name_as_key, '/', $cookie_domain);

//    $has_logout_cookie = $this->openid_connect_sso_default->openid_connect_sso_detect_cookie('logout');
//    if (!empty($has_logout_cookie) && $this->account->isAuthenticated() ) {
//
//      $cookie_domain = $this->openid_connect_sso_default->openid_connect_sso_get_cookie_domain();
//      $name_as_key = $this->openid_connect_sso_default->openid_connect_sso_get_cookie_name('logout', TRUE);
//      $event->getResponse()->headers->clearCookie($name_as_key, '/', $cookie_domain);
//      $this->openid_connect_sso_default->openid_connect_sso_remove_cookie('logout');
//
//      \Drupal::logger('user')->warning('Session closed for %s.', array('%s' => $this->account->getAccountName()));
//      \Drupal::moduleHandler()->invokeAll('user_logout', array($this->account));
//
//      user_logout();
//      $response = new TrustedRedirectResponse('/');
//      $event->setResponse($response);
//
//    }



    $client_name = 'generic';
    $configuration = \Drupal::config('openid_connect.settings.' . $client_name)->get('settings');

    $has_logout_cookie = $this->openid_connect_sso_default->openid_connect_sso_detect_cookie('logout');
    if (!empty($has_logout_cookie) && $this->account->isAuthenticated() ) {

//      $cookie_domain = $this->openid_connect_sso_default->openid_connect_sso_get_cookie_domain();
//      $name_as_key = $this->openid_connect_sso_default->openid_connect_sso_get_cookie_name('logout', TRUE);
//      $this->openid_connect_sso_default->openid_connect_sso_remove_cookie('logout');
//      $event->getResponse()->headers->clearCookie($name_as_key, '/', $cookie_domain);
//
//      \Drupal::logger('user')->warning('Session closed for %s.', array('%s' => $this->account->getAccountName()));
//      \Drupal::moduleHandler()->invokeAll('user_logout', array($this->account));
//
//      user_logout();
//      $response = new TrustedRedirectResponse('/');
//      $event->setResponse($response);

    }

    if (!empty($configuration) && !empty($configuration['authorization_endpoint'])) {
      $url_pieces = parse_url($configuration['authorization_endpoint']);
      if (!empty($url_pieces) && !empty($url_pieces['host']) && $url_pieces['host'] == $_SERVER['HTTP_HOST']) {
        return;
      }
    }



    $anon = !$this->account->isAuthenticated();
    $on_user_page = in_array($url = $event->getRequest()->getRequestUri(), array('/user', '/user/login', '/user/password'));

    $new_login = $anon && $on_user_page && !isset($_GET['admin']);
    $has_cookie = $this->openid_connect_sso_default->openid_connect_sso_detect_cookie('login');

    $path_args = explode('/', ltrim(\Drupal::service('path.current')->getPath(), '/'));
    // This variable indicates that an OAuth2 flow was already started, and the
    // server has just redirected the user back to the redirect callback.
    $in_flow = $path_args[0] == 'openid-connect' && $path_args[1] && isset($_GET['state']);

    $network_login = $has_cookie && !$in_flow;

    if ($network_login && !$anon) {

//      // We're supposed to log the user in, but he's already logged in.
//      // Destroy his current session and log him out first.
//      // This code is the same as user_logout(), but without the redirect.
//      \Drupal::logger('user')->warning('Session closed for %s.', array('%s' => $this->account->getAccountName()));
//
//      \Drupal::moduleHandler()->invokeAll('user_logout', array($this->account));
//      session_destroy();
    }

    if ($new_login || $network_login) {
//      print_r($_SESSION);

//      die(__FUNCTION__);
      $_SESSION['openid_connect_destination'] = '/';
      $destination = \Drupal::destination()->getAsArray();
      if (!in_array($destination['destination'], array('/user', '/user/login', '/user/password'))) {
        $_SESSION['openid_connect_destination'] = $destination['destination'];
      }

      $container = \Drupal::getContainer();
      $service = $container->get('plugin.manager.openid_connect_client.processor');

//      $client_name = 'generic';
//      $configuration = \Drupal::config('openid_connect.settings.' . $client_name)->get('settings');

      $generic = $service->createInstance('generic', $configuration);
      $auth = $generic->authorize();
      $event->setResponse($auth);

    }
  }



  public function onResponseSetCookie(FilterResponseEvent $event) {
    return;
    $anon = !$this->account->isAuthenticated();
    $on_user_page = in_array($url = $event->getRequest()->getRequestUri(), array('/user', '/user/login', '/user/password'));

    $new_login = $anon && $on_user_page && !isset($_GET['admin']);
    $has_cookie = $this->openid_connect_sso_default->openid_connect_sso_detect_cookie('login');

    $path_args = explode('/', ltrim(\Drupal::service('path.current')->getPath(), '/'));
    // This variable indicates that an OAuth2 flow was already started, and the
    // server has just redirected the user back to the redirect callback.
    $in_flow = $path_args[0] == 'openid-connect' && $path_args[1] && isset($_GET['state']);

    $network_login = $has_cookie && !$in_flow;
    if ($new_login || $network_login) {


      $cookie_domain = $this->openid_connect_sso_default->openid_connect_sso_get_cookie_domain();
      $name_as_key = $this->openid_connect_sso_default->openid_connect_sso_get_cookie_name('login', TRUE);
      if (!empty($name_as_key) && array_key_exists($name_as_key, $_COOKIE) && !empty($_COOKIE[$name_as_key])) {

        $expire = time() + 200000 - 30;
        $event->getResponse()->headers->setCookie(new Cookie($name_as_key, 1, $expire, '/', $cookie_domain, true, false));

        $oposite_name_as_key = $this->openid_connect_sso_default->openid_connect_sso_get_cookie_name('logout', TRUE);
        $event->getResponse()->headers->clearCookie($oposite_name_as_key, '/', $cookie_domain);
      }

      $name_as_key = $this->openid_connect_sso_default->openid_connect_sso_get_cookie_name('logout', TRUE);
      if (!empty($name_as_key) && array_key_exists($name_as_key, $_COOKIE) && !empty($_COOKIE[$name_as_key])) {

        $expire = time() + 200000 - 30;
        $event->getResponse()->headers->setCookie(new Cookie($name_as_key, 1, $expire, '/', $cookie_domain, true, false));

        $oposite_name_as_key = $this->openid_connect_sso_default->openid_connect_sso_get_cookie_name('login', TRUE);
        $event->getResponse()->headers->clearCookie($oposite_name_as_key, '/', $cookie_domain);
      }
    }
  }
  /**
   * Registers the methods in this class that should be listeners.
   *
   * @return array
   *   An array of event listener definitions.
   */
  static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = array('onResponseInit', -100033);
    $events[KernelEvents::RESPONSE][] = array('onResponseSetCookie', -100032);
    return $events;
  }


}
