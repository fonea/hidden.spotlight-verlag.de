<?php

/**
 * @file
 * Contains \Drupal\openid_connect_sso_client\Routing\SingleSignOnRouteSubscriber.
 */

namespace Drupal\openid_connect_sso_client\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class SingleSignOnRouteSubscriber extends RouteSubscriberBase {

  /**
   * Constructs a new RouteSubscriber.
   *
   * @param \Drupal\config_translation\ConfigMapperManagerInterface $mapper_manager
   *   The mapper plugin discovery service.
   */
  public function __construct() {

  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {

//    $paths = ['user.register', 'user.pass', 'user.login'];
    $paths = ['user.register', 'user.pass', 'user.login', 'entity.user.edit_form'];

    $disbale_login = \Drupal::config('openid_connect_sso.settings_config')->get('openid_connect_sso_disable_login');

    if (!empty($disbale_login)) {
      foreach($paths as $p) {
        if ($route = $collection->get($p)) {
          if (\Drupal::currentUser()->id() != 1) {
            $route->setRequirement('_access', 'FALSE');
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {

    $events[RoutingEvents::ALTER] = array('onAlterRoutes', 1000);
    return $events;
  }

}
