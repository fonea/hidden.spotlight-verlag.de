<?php

/**
 * @file
 * Contains Drupal\openid_connect_sso\Form\SettingsForm.
 */

namespace Drupal\openid_connect_sso\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 *
 * @package Drupal\openid_connect_sso\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'openid_connect_sso.settings_config'
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    global $base_url;

    $config = $this->config('openid_connect_sso.settings_config');
    $form['openid_connect_sso_enabled'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Enable SSO'),
      '#description' => $this->t(''),
      '#default_value' => $config->get('openid_connect_sso_enabled'),
    );

    $form['openid_connect_sso_script_url'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('SSO script URL'),
      '#description' => $this->t('The first URL to visit in the SSO redirection chain.'),
      '#default_value' => !empty($config->get('openid_connect_sso_script_url')) ? $config->get('openid_connect_sso_script_url') : $base_url.'/sso.php',
      '#states' => array(
        'visible' => array(
          ':input[name="openid_connect_sso_enabled"]' => array('checked' => TRUE),
        ),
      )
    );

    $form['openid_connect_sso_disable_login'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Disable Drupal Login'),
      '#description' => $this->t('Clear Cache.'),
      '#default_value' => $config->get('openid_connect_sso_disable_login'),
    );
    $form['openid_connect_sso_cookie_domain'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Cookie domain'),
      '#description' => $this->t('The domain name to use when clearing SSO cookies. Leave this blank to use the current host name.'),
      '#default_value' => $config->get('openid_connect_sso_cookie_domain'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('openid_connect_sso.settings_config')
      ->set('openid_connect_sso_enabled', $form_state->getValue('openid_connect_sso_enabled'))
      ->set('openid_connect_sso_script_url', $form_state->getValue('openid_connect_sso_script_url'))
      ->set('openid_connect_sso_cookie_domain', $form_state->getValue('openid_connect_sso_cookie_domain'))
      ->set('openid_connect_sso_disable_login', $form_state->getValue('openid_connect_sso_disable_login'))
      ->save();
  }

}
