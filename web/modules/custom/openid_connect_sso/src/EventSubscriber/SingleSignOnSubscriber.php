<?php

/**
 * @file
 * Contains Drupal\openid_connect_sso\SingleSignOnSubscriber.
 */

namespace Drupal\openid_connect_sso\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\openid_connect_sso\SingleSignOnService;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\FinishRequestEvent;
//use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Session\AnonymousUserSession;

//use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\PostResponseEvent;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpFoundation\Cookie;

/**
 * Class SingleSignOnSubscriber.
 *
 * @package Drupal\openid_connect_sso
 */
class SingleSignOnSubscriber implements EventSubscriberInterface
{

  /**
   * Drupal\openid_connect_sso\SingleSignOnService definition.
   *
   * @var Drupal\openid_connect_sso\SingleSignOnService
   */
  protected $openid_connect_sso_default;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * Constructor.
   */
  public function __construct(SingleSignOnService $openid_connect_sso_default, AccountInterface $account)
  {
    $this->openid_connect_sso_default = $openid_connect_sso_default;
    $this->account = $account;
  }

  public function onResponseInit(FilterResponseEvent $event)
  {
    \Drupal::service('page_cache_kill_switch')->trigger();
    if (!empty($_SESSION['oid_connect_sso_start_cycle_login']) ) {
      $is_front_page =  \Drupal::service('path.matcher')->isFrontPage();

      if (!empty($is_front_page)) {
        unset($_SESSION['oid_connect_sso_start_cycle_login']);
        $this->openid_connect_sso_default->openid_connect_sso_set_redirect('login');
      }
    }
//    $c = print_r($_COOKIE, true);
//    \Drupal::logger('onResponseInit')->notice('Cookies: %name.', array('%name' => $c));
//    \Drupal::service('page_cache_kill_switch')->trigger();
//    echo "<div style='background-color: #bcff40;'>";
//    print_r($_COOKIE);
//    echo "</div>";
//    return;
//    die(__FUNCTION__);
//    die(__FUNCTION__);

//    $op = 'login';
//    $login = $this->openid_connect_sso_default->openid_connect_sso_detect_cookie($op);
//    $cookie_domain = $this->openid_connect_sso_default->openid_connect_sso_get_cookie_domain();
//    if (!empty($login)) {
//    \Drupal::service('page_cache_kill_switch')->trigger();
//        return;

//      $name_as_key = $this->openid_connect_sso_default->openid_connect_sso_get_cookie_name($op, TRUE);
//      $expire = time() + 200000 - 30;
//      $event->getResponse()->headers->setCookie(new Cookie($name_as_key, 1, $expire, '/', $cookie_domain, true, false));
//
//      $logout_name_as_key = $this->openid_connect_sso_default->openid_connect_sso_get_cookie_name('logout', TRUE);
//      $event->getResponse()->headers->clearCookie($logout_name_as_key, '/', $cookie_domain);
//    }
//
//    $op = 'logout';
//    $logout = $this->openid_connect_sso_default->openid_connect_sso_detect_cookie($op);
//    if (!empty($logout)) {
//      $name_as_key = $this->openid_connect_sso_default->openid_connect_sso_get_cookie_name($op, TRUE);
//      $expire = time() + 200000 - 30;
//      $event->getResponse()->headers->setCookie(new Cookie($name_as_key, 1, $expire, '/', $cookie_domain, true, false));
//
//      $login_name_as_key = $this->openid_connect_sso_default->openid_connect_sso_get_cookie_name('login', TRUE);
//      $event->getResponse()->headers->clearCookie($login_name_as_key, '/', $cookie_domain);
//    }

//    $event->getResponse()->headers->setCookie(new Cookie('florin', 1));
//    public function __construct($name, $value = null, $expire = 0, $path = '/', $domain = null, $secure = false, $httpOnly = true)
//    $response->headers->setCookie(new Cookie(BigPipeStrategy::NOJS_COOKIE, TRUE, 0, '/', NULL, FALSE, FALSE));

//    $create = 'Drupal.visitor.SSOLogout';
//    $domain = "domain1.local.de";
//    $secure = true;
//    setcookie($create, 1, time() + 200000 - 30, '/', $domain, $secure);

//    $op = 'login';
//    $login = $this->openid_connect_sso_default->openid_connect_sso_detect_cookie($op);
//    if (!empty($login)) {

//    $expire = time() + 200000 - 30;
//    $domain = $_SERVER['HTTP_HOST'];
//    $name_as_key = 'Drupal_visitor_SSOLogin';
//      $event->getResponse()->headers->setCookie(new Cookie($name_as_key, 1, $expire, '/', $domain, true, false));
//    }

//    $name_as_key = 'Drupal_visitor_SSOLoginv';
//    $expire = time() - 3600;
//    $domain = $_SERVER['HTTP_HOST'];
//    $event->getResponse()->headers->setCookie(new Cookie($name_as_key, 1, $expire, '/', $domain, true, false));
//    $event->getResponse()->headers->removeCookie(new Cookie($name_as_key, '', $expire, '/', $domain, true, false));
//    $event->getResponse()->headers->removeCookie($name_as_key, '/', $domain);
//    $v = $event->getResponse()->headers->getCookies();
//die(  '#');
//    if (empty($_GET['par'])) {

//      $url = "https://domain1.local.de/sso.php";
//      $response = new TrustedRedirectResponse($url);
//      $event->setResponse($response);
//    }

//    echo "<div style='background-color: #bcff40;'>";
//    print_r($_COOKIE);
//    echo "</div>";
//    session_destroy();
//    if (isset($_SERVER['HTTP_COOKIE'])) {
//      $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
//      foreach($cookies as $cookie) {
//        $parts = explode('=', $cookie);
//        $name = trim($parts[0]);
//        setcookie($name, '', time()-1000);
//        setcookie($name, '', time()-1000, '/');
//      }
//    }

//    return;
    $login = $this->openid_connect_sso_default->openid_connect_sso_detect_cookie('login');
    if (!empty($login)) {
      \Drupal::service('page_cache_kill_switch')->trigger();
    }
    $needs_logout = $this->account->isAuthenticated() && $this->openid_connect_sso_default->openid_connect_sso_detect_cookie('logout');

    if ($needs_logout) {
      // The user might be trying to log in, having already logged out on a
      // client site. In that case, we want to log the user out and redirect back
      // to the same page.

      $oauth2_login = isset($_GET['client_id'], $_GET['redirect_uri']) && $event->getRequest()->getRequestUri() == '/oauth2/authorize';

      if ($oauth2_login) {
        // Log out the user manually. We can't use user_logout(), because we want
        // to skip its drupal_goto() call.

        \Drupal::logger('user')->warning('Session closed for %s.', array('%s' => $this->account->getAccountName()));
//        \Drupal::moduleHandler()->invokeAll('user_logout', array($this->account));
//        \Drupal::service('session_manager')->destroy();
//        session_destroy();

//        $user = User::load(0);
//        $this->account->setAccount(new AnonymousUserSession());
      } else {
        \Drupal::logger('user')->notice('Session closed for %name.', array('%name' => $this->account->getAccountName()));

        \Drupal::moduleHandler()->invokeAll('user_logout', array($this->account));

        // Destroy the current session, and reset $user to the anonymous user.
        // Note: In Symfony the session is intended to be destroyed with
        // Session::invalidate(). Regrettably this method is currently broken and may
        // lead to the creation of spurious session records in the database.
        // @see https://github.com/symfony/symfony/issues/12375
        \Drupal::service('session_manager')->destroy();
        \Drupal::currentUser()->setAccount(new AnonymousUserSession());
//        $this->account->setAccount(new AnonymousUserSession());
      }
      $this->openid_connect_sso_default->openid_connect_sso_remove_cookie('logout');
    } else {
      // Ensure that the logout cookie is removed.
      $this->openid_connect_sso_default->openid_connect_sso_remove_cookie('logout');
    }
  }

  public function onResponseExit(FilterResponseEvent $event)
  {
    global $base_url;
    $enabled_interface = \Drupal::config('openid_connect_sso.settings_config')->get('openid_connect_sso_enabled');
    $enabled_install = \Drupal::config('openid_connect_sso.settings')->get('enabled');
    if (empty($enabled_interface) || empty($enabled_install)) {
      return;
    }
    $redirect = $this->openid_connect_sso_default->openid_connect_sso_get_redirect();
    if (empty($redirect)) {
      return;
    }

    $request_uri = $event->getRequest()->getRequestUri();
    $url_object = \Drupal::service('path.validator')->getUrlIfValid($request_uri);
    if (!empty($url_object) && is_object($url_object)) {
      $route_name = $url_object->getRouteName();
      if (!empty($route_name) && $route_name == 'user.reset' ) {
        return;
      }
    }

    $url_options = array(
      'absolute' => TRUE,
      'query' => array(
        'op' => $redirect,
        'origin_host' => $_SERVER['HTTP_HOST'],
      ),
    );

    $client_name = 'generic';
    $configuration = \Drupal::config('openid_connect.settings.' . $client_name)->get('settings');
    if ($redirect == 'login') {

      // The $destination parameter is only set if this is invoked via
      // drupal_goto().
      $options = array('absolute' => TRUE);
      if (isset($_GET['destination'])) {
        $path = $_GET['destination'];
      } else {
        $path = $event->getRequest()->getRequestUri();
        if ($query = UrlHelper::filterQueryParameters($event->getRequest()->query->all())) {
          $options['query'] = $query;
        }
      }
      $valid = \Drupal::pathValidator()->getUrlIfValid($path);
      if (!empty($valid)) {
        $destination = Url::fromUserInput($path, $options)->toString();
      }

      if (!empty($configuration) && !empty($configuration['authorization_endpoint'])) {
        $url_pieces = parse_url($configuration['authorization_endpoint']);
        if (!empty($url_pieces) && !empty($url_pieces['host']) && $url_pieces['host'] == $_SERVER['HTTP_HOST']) {

          if ($path != '/oauth2/authorize') {
            $destination = Url::fromUserInput('/', ['absolute' => true])->toString();
          }
        }
      }

      // A core bug sets $destination to user/login when user/$uid is
      // requested instead. Even though that works, we set the proper url
      // here for clarity sake.
      $login_url = new Url('user.login');
      $login_url->setAbsolute(TRUE);

//      var_dump($destination);
      if (empty($destination) || $destination == $login_url->toString()) {
        $destination = Url::fromUserInput('/', ['absolute' => true])->toString();
//        $destination = Url::fromUserInput('/user', ['absolute' => true])->toString();
      }

      // Set the destination to which the SSO script will return the user.
      $url_options['query']['destination'] = $destination;
    }
    if ($redirect == 'logout') {
//      if (!empty($configuration) && !empty($configuration['authorization_endpoint'])) {
//        $url_pieces = parse_url($configuration['authorization_endpoint']);
//        if (!empty($url_pieces) && !empty($url_pieces['host']) && $url_pieces['host'] == $_SERVER['HTTP_HOST']) {
//          $url_options['query']['destination'] = $base_url.'/goodbye';
//        }
//      }

      $path_destination_logout = 'https://sso.guj.de/spotlight_shop/auth/logout?1=1&redirect_url=https://shop.spotlight-verlag.de/gujsso/auth/logout/';
      $url_options['query']['destination'] = $path_destination_logout;
    }

    $sso_script_url = $this->openid_connect_sso_default->openid_connect_sso_get_script_url();
    $url = Url::fromUri($sso_script_url, $url_options)->toString();

    $response = new TrustedRedirectResponse($url);
    $event->setResponse($response);
//    $_SESSION['oid_connect_sso_login_redirect'] = $redirect;
  }

  public function onResponseFinal(FilterResponseEvent $event)
  {
    return;
    $enabled_interface = \Drupal::config('openid_connect_sso.settings_config')->get('openid_connect_sso_enabled');
    $enabled_install = \Drupal::config('openid_connect_sso.settings')->get('enabled');
    if (empty($enabled_interface) || empty($enabled_install)) {
      return;
    }
    $redirect = $this->openid_connect_sso_default->openid_connect_sso_get_redirect();
    if (empty($redirect)) {
      return;
    }

    $request_uri = $event->getRequest()->getRequestUri();
    $url_object = \Drupal::service('path.validator')->getUrlIfValid($request_uri);
    if (!empty($url_object) && is_object($url_object)) {
      $route_name = $url_object->getRouteName();
      if (!empty($route_name) && $route_name == 'user.reset' ) {
        return;
      }
    }

    $cookie_domain = $this->openid_connect_sso_default->openid_connect_sso_get_cookie_domain();

    if ($redirect == 'login') {
//      $name_as_key = $this->openid_connect_sso_default->openid_connect_sso_get_cookie_name('login', TRUE);
//      $expire = time() + 200000 - 30;
//      $event->getResponse()->headers->setCookie(new Cookie($name_as_key, 1, $expire, '/', $cookie_domain, true, false));
//
//      $oposite_name_as_key = $this->openid_connect_sso_default->openid_connect_sso_get_cookie_name('logout', TRUE);
//      $event->getResponse()->headers->clearCookie($oposite_name_as_key, '/', $cookie_domain);
    } elseif ($redirect == 'logout') {

//      $name_as_key = $this->openid_connect_sso_default->openid_connect_sso_get_cookie_name('logout', TRUE);
//      $expire = time() + 200000 - 30;
//      $event->getResponse()->headers->setCookie(new Cookie($name_as_key, 1, $expire, '/', $cookie_domain, true, false));
//
//      $oposite_name_as_key = $this->openid_connect_sso_default->openid_connect_sso_get_cookie_name('login', TRUE);
//      $event->getResponse()->headers->clearCookie($oposite_name_as_key, '/', $cookie_domain);

    } else {
      return;
    }

  }

  public function onController(FilterControllerEvent $event)
  {
    global $base_url;

    $uri = $event->getRequest()->getRequestUri();
    $client_id = $event->getRequest()->query->get('client_id');
    $redirect_uri = $event->getRequest()->query->get('redirect_uri');
    $response_type = $event->getRequest()->query->get('response_type');

    if (empty($uri) || empty($client_id) || empty($redirect_uri) || empty($response_type)) {
      return;
    }

    if ($response_type == 'code' && empty($this->account->isAuthenticated())) {
      $container = \Drupal::getContainer();
      $em = $container->get('entity.manager');

      $client = $em->getStorage('oauth2_server_client')->loadByProperties(['client_id' => $client_id]);
      if (!empty($client)) {
        $_SESSION['oid_connect_sso_start_process_login'] = $base_url;
      }
    }
  }

  public function onKernelTerminate(PostResponseEvent $event)
  {
  }

  public function finishRequest(FinishRequestEvent $event)
  {
//    $c = print_r($_COOKIE, true);
//    \Drupal::logger('finishRequest')->notice('Cookies: %name.', array('%name' => $c));
    return;
    if (empty($this->account->isAuthenticated()) ) {
      $login = $this->openid_connect_sso_default->openid_connect_sso_detect_cookie('login');
      if (!empty($login)) {
        $name = $this->openid_connect_sso_default->openid_connect_sso_get_cookie_name('login', TRUE);
        $this->openid_connect_sso_default->openid_connect_sso_remove_cookie('login');
        $event->getRequest()->cookies->remove($name);
      }
    }
    if (!empty($this->account->isAuthenticated()) ) {
      $logout = $this->openid_connect_sso_default->openid_connect_sso_detect_cookie('logout');
      if (!empty($logout)) {
        $name = $this->openid_connect_sso_default->openid_connect_sso_get_cookie_name('logout', TRUE);
        $this->openid_connect_sso_default->openid_connect_sso_remove_cookie('logout');
        $event->getRequest()->cookies->remove($name);
      }
    }
  }

  public function onResponseClearCookies(FilterResponseEvent $event)
  {
    return;
    $cookie_domain = $this->openid_connect_sso_default->openid_connect_sso_get_cookie_domain();
    if (empty($this->account->isAuthenticated()) ) {
      $login = $this->openid_connect_sso_default->openid_connect_sso_detect_cookie('login');
      if (!empty($login)) {
        $name = $this->openid_connect_sso_default->openid_connect_sso_get_cookie_name('login', TRUE);
        $event->getResponse()->headers->clearCookie($name, '/', $cookie_domain);
        $this->openid_connect_sso_default->openid_connect_sso_remove_cookie('login');
      }
    }

    if (!empty($this->account->isAuthenticated()) ) {
      $logout = $this->openid_connect_sso_default->openid_connect_sso_detect_cookie('logout');
      if (!empty($logout)) {
        $name = $this->openid_connect_sso_default->openid_connect_sso_get_cookie_name('logout', TRUE);
        $event->getResponse()->headers->clearCookie($name, '/', $cookie_domain);
        $this->openid_connect_sso_default->openid_connect_sso_remove_cookie('logout');
      }
    }
  }

  /**
   * Registers the methods in this class that should be listeners.
   *
   * @return array
   *   An array of event listener definitions.
   */
  static function getSubscribedEvents()
  {
    $events[KernelEvents::CONTROLLER][] = array('onController', 100030);
    $events[KernelEvents::RESPONSE][] = array('onResponseInit', -100023);
    $events[KernelEvents::RESPONSE][] = array('onResponseExit', -100027);
    $events[KernelEvents::RESPONSE][] = array('onResponseClearCookies', -110028);
    $events[KernelEvents::RESPONSE][] = array('onResponseFinal', -100025);

    $events[KernelEvents::FINISH_REQUEST][] = array('finishRequest', 100027);
    $events[KernelEvents::TERMINATE][] = array('onKernelTerminate', 100026);
    return $events;
  }
}
