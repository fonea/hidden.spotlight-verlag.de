<?php

/**
 * @file
 * Contains Drupal\openid_connect_sso\SingleSignOnService.
 */

namespace Drupal\openid_connect_sso;


/**
 * Class SingleSignOnService.
 *
 * @package Drupal\openid_connect_sso
 */
class SingleSignOnService implements SingleSignOnServiceInterface {



  /**
   * Constructor.
   */
  public function __construct() {

  }
  /**
   * Detect whether a valid cookie is set for the correct domain.
   *
   * @param string $op
   *   The operation: 'logout' or 'login'.
   *
   * @return bool
   *   TRUE if the cookie is set, FALSE if not.
   */
  function openid_connect_sso_detect_cookie($op) {
    $name_as_key = $this->openid_connect_sso_get_cookie_name($op, TRUE);

    if (!empty($_COOKIE[$name_as_key])) {
      return TRUE;
    }
    $with_domain = $this->openid_connect_sso_get_cookie_name($op, TRUE, TRUE);
    return !empty($_COOKIE[$with_domain]);
  }
  /**
   * Get the cookie name for a given operation.
   *
   * @param string $op
   *   The operation: 'logout' or 'login'.
   * @param bool $as_key
   *   Whether to return the name as appropriate for an array key in $_COOKIE,
   *   i.e. with '.' replaced with '_'.
   * @param bool $with_domain
   *   Whether to add the domain name to the cookie name.
   *
   * @throws InvalidArgumentException for an invalid $op
   *
   * @return string
   */
  function openid_connect_sso_get_cookie_name($op, $as_key = FALSE, $with_domain = FALSE) {
    switch ($op) {
      case 'logout':
        $cookie_name = 'Drupal.visitor.SSOLogout';
        break;

      case 'login':
        $cookie_name = 'Drupal.visitor.SSOLogin';
        break;

      default:
        throw new InvalidArgumentException('Invalid operation: ' . $op);
    }
    if ($with_domain) {
      $cookie_name .= '_' . $this->openid_connect_sso_get_cookie_domain();
    }
    if ($as_key) {
      return str_replace('.', '_', $cookie_name);
    }
    return $cookie_name;
  }

  /**
   * Get the configured cookie domain.
   *
   * @return string
   */
  function openid_connect_sso_get_cookie_domain() {

    $cookie_domain = \Drupal::config('openid_connect_sso.settings_config')->get('openid_connect_sso_cookie_domain');
    if (empty($cookie_domain)) {
      $cookie_domain = $_SERVER['HTTP_HOST'];
    }
    return $cookie_domain;
  }

  /**
   * Sets an SSO redirect to be performed on page shutdown.
   *
   * @param $type
   *   The type of redirect to perform. "login" or "logout".
   */
  function openid_connect_sso_set_redirect($type = NULL) {
    $redirect = &drupal_static(__FUNCTION__, NULL);
    if (isset($type)) {
      $redirect = $type;
    }

    return $redirect;
  }

  /**
   * Ensure that a cookie is removed for the correct domain.
   *
   * @param string $op
   */
  function openid_connect_sso_remove_cookie($op) {
    $cookie_name = $this->openid_connect_sso_get_cookie_name($op);
    $name_as_key = $this->openid_connect_sso_get_cookie_name($op, TRUE);
    $cookie_domain = $this->openid_connect_sso_get_cookie_domain();

    if (isset($_COOKIE[$name_as_key])) {
      setcookie($cookie_name, '', time() - 3600, '/', $cookie_domain);
      unset($_COOKIE[$name_as_key]);
    }
    $extended_name_as_key = $name_as_key . '_' . str_replace('.', '_', $cookie_domain);
    if (isset($_COOKIE[$extended_name_as_key])) {
      $extended_name = $cookie_name . '_' . $cookie_domain;
      setcookie($extended_name, '', time() - 3600, '/', $cookie_domain);
      unset($_COOKIE[$extended_name_as_key]);
    }
  }

  /**
   * Returns the SSO redirect to be performed on page shutdown, if any.
   */
  function openid_connect_sso_get_redirect() {
    return $this->openid_connect_sso_set_redirect();
  }

  /**
   * Returns the SSO script url based on the configured location.
   */
  function openid_connect_sso_get_script_url() {
    global $base_url;

    $url = \Drupal::config('openid_connect_sso.settings_config')->get('openid_connect_sso_script_url');
    if (empty($url)) {
      $url = $base_url . '/sso.php';
    }

    return $url;
  }

}
